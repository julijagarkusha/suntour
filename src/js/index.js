const linkAdd = () => {
  const headElement = document.querySelector('head');
  const newLinkElement = document.createElement('link');
  newLinkElement.href = "styles/index.css";
  newLinkElement.rel = "stylesheet";
  headElement.appendChild(newLinkElement);
};

document.addEventListener("DOMContentLoaded", linkAdd);